resource "kubernetes_namespace" "confy-frontend" {
  metadata {
    name = "confy-frontend"
  }
}

resource "helm_release" "confy-frontend" {
  name      = "confy-frontend"
  namespace = kubernetes_namespace.confy-frontend.metadata[0].name
  chart     = "charts/confy-frontend"

  values = [
    file("helm/confy-frontend.yaml")
  ]
}
