locals {
  helm_repository_bitnami     = "https://charts.bitnami.com/bitnami"
  helm_repository_codecentric = "https://codecentric.github.io/helm-charts"
  helm_repository_neo4j       = "https://neo4j-contrib.github.io/neo4j-helm/"
  helm_repository_stable      = "https://kubernetes-charts.storage.googleapis.com"
}
