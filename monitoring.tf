resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = "monitoring"
  }
}
resource "helm_release" "monitoring" {
  chart = "stable/prometheus-operator"
  #repository = local.helm_repository_stable
  name      = "prometheus"
  version   = "9.3.1"
  namespace = kubernetes_namespace.monitoring.metadata[0].name
  timeout   = 600
  values = [
    file("helm/monitoring.yaml")
  ]
}

resource "kubernetes_config_map" "spring-boot-dashboard" {
  metadata {
    name = "spring-boot-dashboard"
    labels = {
      "grafana_dashboard" : "spring-boot-dashboard"
    }
    namespace = "monitoring"
  }
  data = {
    "spring-boot-dashboard.json" : file("dashboards/dashboard.json")
  }
}

