variable "keycloak_namespace" {
  type        = string
  default     = "keycloak"
  description = "The namespace where to put keycloak in"
}

variable "development" {
  type        = bool
  default     = true
  description = "Activates some development features"
}
