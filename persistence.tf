resource "kubernetes_namespace" "persistence" {
  metadata {
    name = "persistence"
  }
}

resource "helm_release" "neo4j" {
  chart     = "https://github.com/neo4j-contrib/neo4j-helm/releases/download/4.1.0-1/neo4j-4.1.0-1.tgz"
  namespace = kubernetes_namespace.persistence.metadata[0].name
  name      = "neo4j"

  version = "4.1.0-1"

  values = [
    file("helm/neo4j.yaml")
  ]
}

resource "kubernetes_service" "neo4j-service-nodeport" {
  metadata {
    name      = "neo4j-neo4j-nodeport"
    namespace = kubernetes_namespace.persistence.metadata[0].name
  }

  count = var.development ? 1 : 0

  spec {
    type = "NodePort"

    port {
      name        = "http"
      port        = 7474
      protocol    = "TCP"
      target_port = 7474
      node_port   = 32000
    }

    port {
      name        = "bolt"
      port        = 7687
      protocol    = "TCP"
      target_port = "7687"
      node_port   = 32001
    }

    selector = {
      "app.kubernetes.io/component" = "core"
      "app.kubernetes.io/instance"  = "neo4j"
      "app.kubernetes.io/name"      = "neo4j"
    }
    publish_not_ready_addresses = true
    session_affinity            = "None"
  }
}

