resource "kubernetes_namespace" "confy-backend" {
  metadata {
    name = "confy-backend"
  }
}


resource "kubernetes_secret" "confy-backend-tls" {
  metadata {
    name      = "confy-backend-tls"
    namespace = kubernetes_namespace.confy-backend.metadata[0].name
  }

  data = {
    "tls.key" = file("tls/key.pem")
    "tls.crt" = file("tls/cert.pem")
  }

  type = "kubernetes.io/tls"

}


resource "helm_release" "confy-backend" {

  depends_on = [kubernetes_secret.confy-backend-tls]

  name      = "confy-backend"
  namespace = kubernetes_namespace.confy-backend.metadata[0].name
  chart     = "charts/confy-backend"

  values = [
    file("helm/confy-backend.yaml")
  ]
}
